﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace TodoApp.Migrations
{
    public partial class addInitialUsers : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("INSERT INTO [dbo].[Users] ([Name], [Age], [IsAdmin]) VALUES('John', 25, 'false')");
            migrationBuilder.Sql("INSERT INTO [dbo].[Users] ([Name], [Age], [IsAdmin]) VALUES('Judy', 35, 'true')");
            migrationBuilder.Sql("INSERT INTO [dbo].[Users] ([Name], [Age], [IsAdmin]) VALUES('Oliver', 45, 'false')");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("DELETE FROM[dbo].[Users] WHERE Name = 'John' AND Age = 25");
            migrationBuilder.Sql("DELETE FROM[dbo].[Users] WHERE Name = 'Judy' AND Age = 35");
            migrationBuilder.Sql("DELETE FROM[dbo].[Users] WHERE Name = 'Oliver' AND Age = 45");
        }
    }
}
