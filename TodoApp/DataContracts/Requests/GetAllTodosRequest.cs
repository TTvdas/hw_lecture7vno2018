﻿using TodoApp.DataContracts.Enums;

namespace TodoApp.DataContracts.Requests
{
    public class GetAllTodosRequest
    {
        public string Query { get; set; }

        public bool? Completed { get; set; }

        public SortDirection? SortDirection { get; set; }

        public int? Page { get; set; }
    }
}
