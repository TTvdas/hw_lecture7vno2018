﻿
namespace TodoApp.DataContracts.Requests
{
    public class GetAllUsersRequest
    {
        public int? AgeFrom { get; set; }

        public int? AgeTo { get; set; }

        public bool? Ordered { get; set; }
    }
}
