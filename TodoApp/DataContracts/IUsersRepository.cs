﻿using System.Collections.Generic;
using TodoApp.Data.Entities;
using TodoApp.DataContracts.Requests;

namespace TodoApp.DataContracts
{
    public interface IUsersRepository
    {
        List<User> GetAll(GetAllUsersRequest request);

        User Create(CreateUserRequest request);

        void Update(int id, UpdateUserRequest request);

        void Delete(int id);
    }
}
