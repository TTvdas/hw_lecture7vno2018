﻿using System;
using System.Collections.Generic;
using System.Linq;
using TodoApp.Data.Entities;
using TodoApp.DataContracts;
using TodoApp.DataContracts.Requests;

namespace TodoApp.Data
{
    public class UsersRepository : IUsersRepository
    {
        private readonly AppDbContext _dbContext;
        public UsersRepository(AppDbContext appDbContext)
        {
            _dbContext = appDbContext;
        }

        public List<User> GetAll(GetAllUsersRequest request)
        {
            var query = _dbContext.Users.AsQueryable();

            if(request.AgeFrom.HasValue)
            {
                query = query.Where(user => user.Age >= request.AgeFrom);
            }

            if (request.AgeTo.HasValue)
            {
                query = query.Where(user => user.Age <= request.AgeTo);
            }

            if (request.Ordered.HasValue && (bool)request.Ordered)
            {
                query = query.OrderBy(user => user.Name).ThenBy(user => user.Age);
            }

            return query.ToList();
        }

        public User Create(CreateUserRequest request)
        {
            User user = new User
            {
                Name = request.Name,
                Age = (int)request.Age,
                IsAdmin = (bool)request.IsAdmin
            };

            _dbContext.Users.Add(user);
            _dbContext.SaveChanges();

            return user;
        }

        public void Update(int id, UpdateUserRequest request)
        {
            var user = _dbContext.Users.FirstOrDefault(x => x.Id == id);

            if(user == null)
            {
                throw new Exception($"User with id: {id} could not be found");
            }

            user.Name = request.Name;
            user.Age = (int)request.Age;
            user.IsAdmin = (bool)request.IsAdmin;

            _dbContext.SaveChanges();
        }

        public void Delete(int id)
        {
            var user = _dbContext.Users.FirstOrDefault(x => x.Id == id);

            if(user == null)
            {
                throw new Exception($"User with id: {id} could not be found");
            }

            _dbContext.Users.Remove(user);
            _dbContext.SaveChanges();
        }
    }
}
