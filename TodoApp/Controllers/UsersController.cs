﻿using Microsoft.AspNetCore.Mvc;
using System;
using TodoApp.DataContracts;
using TodoApp.DataContracts.Requests;

namespace TodoApp.Controllers
{
    [Produces("application/json")]
    [Route("api/Users")]
    public class UsersController : Controller
    {
        private readonly IUsersRepository usersRepository;

        public UsersController(IUsersRepository usersRepository)
        {
            this.usersRepository = usersRepository;
        }

        [HttpGet]
        public IActionResult GetAll([FromQuery] GetAllUsersRequest request)
        {
            return Ok(usersRepository.GetAll(request));
        }

        [HttpPost]
        public IActionResult CreateUser([FromBody] CreateUserRequest request)
        {
            if (ModelState.IsValid)
            {
                return Ok(usersRepository.Create(request));
            }
            else
            {
                return BadRequest(ModelState);
            }
        }

        [HttpPut("{id}")]
        public IActionResult UpdateUser(int id, [FromBody] UpdateUserRequest request)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    usersRepository.Update(id, request);
                    return Ok();
                }
                catch (Exception ex)
                {
                    return BadRequest(ex.Message);
                }
            }
            else
            {
                return BadRequest(ModelState);
            }

        }

        [HttpDelete("{id}")]
        public IActionResult DeleteUser(int id)
        {
            try
            {
                usersRepository.Delete(id);
                return Ok();
            }
            catch(Exception ex)
            {
                return NotFound(ex.Message);
            }
        }
    }
}