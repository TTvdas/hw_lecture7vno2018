import React from "react";

export default class TodoItem extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            editEnabled: false,
            editableTitle: this.props.title
        };

        this.onHandleChange = this.onHandleChange.bind(this);
        this.onRemoveTask = this.onRemoveTask.bind(this);
        this.onRenameTask = this.onRenameTask.bind(this);
        this.onToggleTaskEdit = this.onToggleTaskEdit.bind(this);
        this.onToggleTaskStatus = this.onToggleTaskStatus.bind(this);
    }

    render () {
        const toggleIconClass = `glyphicon ${this.props.completed ? 'glyphicon-ok' : 'glyphicon-remove'}`;

        return (
            <div className="todo-item list-group-item">
                <i className="todo-item-status"
                   onClick={this.onToggleTaskStatus}>
                    <span className={toggleIconClass}></span>
                </i>

                <div className="todo-item-text-container">
                    {this.state.editEnabled ? this.renderTaskTitleEdit() : this.renderTaskTitle()}
                </div>
                <button className="todo-item-delete"
                        onClick={this.onRemoveTask}>
                    <span className="glyphicon glyphicon-remove remove-button"></span>
                </button>

            </div>
        );
    }

    renderTaskTitle() {
        return (
            <span onDoubleClick={this.onToggleTaskEdit}>
                {this.state.editableTitle}
            </span>
        );
    }

    renderTaskTitleEdit() {
        return (
            <form onSubmit={this.onRenameTask}>
                <input type="text"
                       className='todo-item-edit-input'
                       value={this.state.editableTitle}
                       onChange={this.onHandleChange}/>
                <input type="submit" hidden/>
            </form>
        );

    }

    onRemoveTask() {
        this.props.onRemoveTask(this.props.index);
    }

    onToggleTaskStatus() {
        const item = {
            id: this.props.index,
            title: this.state.editableTitle,
            completed: !this.props.completed
        };

        this.props.onUpdateTask(item);

        this.setState({
            editEnabled: false
        });
    }

    onToggleTaskEdit() {
        this.setState({
            editEnabled: true
        });
    }

    onRenameTask(e) {
        e.preventDefault();
        const item = {
            id: this.props.index,
            title: this.state.editableTitle,
            completed: this.props.completed
        };

        this.props.onUpdateTask(item);

        this.setState({
            editEnabled: false
        });
    }

    onHandleChange(e) {
        this.setState({
            editableTitle: e.target.value
        });
    }

};