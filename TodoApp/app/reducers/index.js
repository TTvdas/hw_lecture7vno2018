import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';

import todoItems from './todoItems';
import appSettings from './appSettings';

const rootReducer = combineReducers({todoItems, appSettings, routing: routerReducer});

export default rootReducer;